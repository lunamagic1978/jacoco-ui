import axios from 'axios';

export const getGitlabTree = (path) => axios.get(`/tree?path=${path}`);