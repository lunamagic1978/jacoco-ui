import Vue from 'vue';
import VueRouter from 'vue-router';
import VueResource from 'vue-resource'

Vue.use(VueRouter);
Vue.use(VueResource);

// const First = { template: '<div><h2>我是第 1 个子页面</h2></div>' }
// import secondcomponent from '../component/secondcomponent.vue'
import home from '../views/home/views/home.vue'

const router = new VueRouter(
  {

    routes: [
      // {
      //   path: '/first',
      //   component: First
      // },
      // {
      //   path: '/second',
      //   component: secondcomponent
      // },
      {
        path: '/',
        component: home
      }
    ]
  }
);

export default router;
