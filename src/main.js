import Vue from 'vue'
import App from './App.vue'
import VueRouter from "vue-router";
import VueResource from 'vue-resource'
import router from './router'
import codemirror from './components/codemirror';
Vue.use(codemirror)
Vue.use(VueRouter);
Vue.use(VueResource);
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:8080/'

new Vue({
  router: router,
  render: h => h(App)
}).$mount('#app')
