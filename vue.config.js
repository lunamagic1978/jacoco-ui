module.exports = {
  devServer: {
    proxy: {
      '/api': {
        target: process.env.PROXY || 'http://localhost:8080/',
        changeOrigin: true,
        pathRewrite: {
          '^/': ''  // rewrite path
      },
      },
    },
  },
  chainWebpack: (config) => {
    config.resolve.alias
      .set('vue$', 'vue/dist/vue.esm.js');
    const svgRule = config.module.rule('svg');
    svgRule.uses.clear();
    svgRule
      .use('svg-sprite-loader')
      .loader('svg-sprite-loader')
      .options({
        symbolId: 'icon-[name]',
      });
  },
};
